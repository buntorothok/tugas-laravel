@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">{{$question->judul}}</h3>
      <form action="/pertanyaan/{{$question->id}}" method="POST" class="float-right">
        @csrf
        @method('DELETE')
        <input type="submit" class="btn btn-danger btn-sm ml-1" value="Delete">
        </form>
      <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-warning btn-sm float-right ml-1">Edit</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <p>{{$question->isi}}</p>        
    </div>
    <!-- /.card-body -->
  </div>    
@endsection