<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
        $questions = DB::table('pertanyaan')->get();
        return view('forum.pertanyaan', compact('questions'));
    }

    public function create(){
        return view('forum.create');
    }

    public function store(Request $request){
        $profil_id = 1;
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "profil_id" => $profil_id
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dibuat');
    }

    public function show($id){
        $question = DB::table('pertanyaan')
                ->where('id', $id)
                ->first();
        return view('forum.show', compact('question'));
    }

    public function edit($id){
        $question = DB::table('pertanyaan')
                ->where('id', $id)
                ->first();
        return view('forum.edit', compact('question'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')
                -> where('id', $id)
                -> update([
                    'judul' => $request->judul,
                    'isi' => $request->isi,
                    'profil_id' => 1
                ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diedit');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')
                ->where('id', $id)
                ->delete();
        
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus');
    }
}
