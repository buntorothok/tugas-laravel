<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('register');
    }

    public function submit(Request $request){
        $firstname = $request["firstname"];
        $lastname = $request["lastname"];
        // logic register di sini
        if($firstname == "" OR $lastname == ""){
            echo "Firstname dan Lastname tidak boleh kosong ya kak.<br>Kembali ke <a href='/register'>registrasi</a>";
        }else{
            // logic ok, arahkan ke welcome page
            return view('welcome', compact('firstname', 'lastname'));
        }
    }
}
